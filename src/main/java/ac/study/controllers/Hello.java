package ac.study.controllers;

import ac.study.config.ApplicationProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by caiyj on 2017/6/27.
 */
@RestController
public class Hello
{
    @Autowired
    private ApplicationProperties config;

    @RequestMapping("/hello")
    public String greet()
    {
        return "Hello World "+config.evn;
    }
}
