package ac.study.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by caiyj on 2017/6/27.
 */
@Component
public class ApplicationProperties
{
    @Value("${evn}")
    public String evn;
}
